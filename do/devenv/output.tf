# output "devenv_ip" {
#     value = digitalocean_droplet.devenv.ipv4_address
# }

output "fixed_ip" {
  value = digitalocean_floating_ip.devenv.ip_address
}