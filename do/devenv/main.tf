
resource "digitalocean_vpc" "devenv_vpc" {
  name   = "devenv-vpc"
  region = "lon1"
}

resource "digitalocean_volume" "devenv" {
  region                  = "lon1"
  name                    = "devenv"
  size                    = 20
  initial_filesystem_type = "ext4"
  description             = "storage for development pipeline"
}

resource "digitalocean_floating_ip" "devenv" {
  region     = "lon1"
}

resource "digitalocean_record" "jenkins" {
  domain = data.digitalocean_domain.devenv.name
  type   = "A"
  name   = "jenkins"
  value  = digitalocean_floating_ip.devenv.ip_address
}

resource "digitalocean_record" "sonar" {
  domain = data.digitalocean_domain.devenv.name
  type   = "A"
  name   = "sonar"
  value  = digitalocean_floating_ip.devenv.ip_address
}

resource "digitalocean_record" "registry" {
  domain = data.digitalocean_domain.devenv.name
  type   = "A"
  name   = "registry"
  value  = digitalocean_floating_ip.devenv.ip_address
}

resource "digitalocean_record" "baget" {
  domain = data.digitalocean_domain.devenv.name
  type   = "A"
  name   = "baget"
  value  = digitalocean_floating_ip.devenv.ip_address
}

# resource "digitalocean_droplet" "devenv" {
#   image  = "ubuntu-21-04-x64"
#   name   = "devenv"
#   region = "lon1"
#   size   = "s-4vcpu-8gb"
#   vpc_uuid   = digitalocean_vpc.devenv_vpc.id
#   monitoring = "true"
#   ssh_keys   = [data.digitalocean_ssh_key.ssh_key.id]
#   user_data = templatefile("scripts/setup.sh", {
#     USERNAME       = "devenv"
#     PASSWORD       = var.password
#     HOME_DIRECTORY = "/home/devenv"
#     VOLUME         = "/mnt/devenv"
#     POSTGRES_PASSWORD = var.postgres_password    
#     SONAR_DB_PWD   = var.sonar_db_password
#     REGISTRY_PWD   = var.registry_password
#     BAGET_PWD      = var.baget_password
#     BAGET_APIKEY   = var.baget_apikey
#     BAGET_DB_PWD   = var.baget_db_password
#   })
# }

# resource "digitalocean_volume_attachment" "devenv_volume" {
#   droplet_id = digitalocean_droplet.devenv.id
#   volume_id  = digitalocean_volume.devenv.id
# }

# resource "digitalocean_floating_ip_assignment" "devenv" {
#   ip_address = digitalocean_floating_ip.devenv.ip_address
#   droplet_id = digitalocean_droplet.devenv.id
# }

# resource "digitalocean_project" "devenv" {
#   name        = "DevEnv"
#   description = "Development environment tools."
#   purpose     = "Web Applications"
#   environment = "Development"
#   resources   = [digitalocean_droplet.devenv.urn]
# }

