
variable "do_token" {
    description = "Digital ocean token"
}

data "digitalocean_domain" "devenv" {
  name = "nakedelement.co.uk"
}

variable "password" {
    description = "devenv user password"
}

data "digitalocean_ssh_key" "ssh_key" {
  name = "Paul"
}

variable "postgres_password" {
    description = "PostgreSQL root password"
}

variable "sonar_db_password" {
    description = "Sonar Database Password"
}

variable "registry_password" {
    description = "Registry Password"
}

variable "baget_password" {
    description = "Baget Password"
}

variable "baget_apikey" {
    description = "Baget Apikey"
}

variable "baget_db_password" {
    description = "Baget database Password"
}
