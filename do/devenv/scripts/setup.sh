#!/bin/bash
set -euo pipefail

# Add sudo user and grant privileges
useradd ${USERNAME} --password $(openssl passwd -1 ${PASSWORD}) --create-home --shell "/bin/bash" --groups sudo

# Create SSH directory for sudo user
mkdir --parents ${HOME_DIRECTORY}/.ssh

cp /root/.ssh/authorized_keys ${HOME_DIRECTORY}/.ssh

# Adjust SSH configuration ownership and permissions
chmod 0700 ${HOME_DIRECTORY}/.ssh
chmod 0600 ${HOME_DIRECTORY}/.ssh/authorized_keys
chown --recursive ${USERNAME}:${USERNAME} ${HOME_DIRECTORY}/.ssh

# Disable root SSH login with password
sed --in-place 's/^PermitRootLogin.*/PermitRootLogin no\
AllowUsers '${USERNAME}'/g' /etc/ssh/sshd_config
systemctl restart sshd

##########
# Docker #
##########
apt-get update
apt-get -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common postgresql-client-common postgresql-client-13

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io

usermod -aG docker ${USERNAME}

sudo chown ${USERNAME}:docker /var/run/docker.sock
service docker start

##################
# Docker Compose #
##################
curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

###########
# Dev Env #
###########
apt-get install -y unzip
cd ${HOME_DIRECTORY}

wget https://bitbucket.org/findmytea/devenv/get/develop.zip
unzip develop.zip
rm develop.zip

mv findmytea-devenv-* dev-pipeline

echo POSTGRES_PASSWORD=${POSTGRES_PASSWORD} >> ${HOME_DIRECTORY}/dev-pipeline/.env
echo SONAR_DB_PWD=${SONAR_DB_PWD} >> ${HOME_DIRECTORY}/dev-pipeline/.env
echo REGISTRY_PWD=${REGISTRY_PWD} >> ${HOME_DIRECTORY}/dev-pipeline/.env
echo BAGET_APIKEY=${BAGET_APIKEY} >> ${HOME_DIRECTORY}/dev-pipeline/.env
echo BAGET_PWD=${BAGET_PWD} >> ${HOME_DIRECTORY}/dev-pipeline/.env
echo BAGET_DB_PWD=${BAGET_DB_PWD} >> ${HOME_DIRECTORY}/dev-pipeline/.env

cd dev-pipeline 

chmod +x ${HOME_DIRECTORY}/dev-pipeline/setup.sh
${HOME_DIRECTORY}/dev-pipeline/setup.sh
echo "#!/bin/bash" >> /etc/rc.local
echo ${HOME_DIRECTORY}/dev-pipeline/setup.sh >> /etc/rc.local
echo chown ${USERNAME}:docker /var/run/docker.sock >> /etc/rc.local
chmod +x /etc/rc.local

mkdir -p ${HOME_DIRECTORY}/registry
mkdir -p ${HOME_DIRECTORY}/baget

if [[ $(mount | grep /mnt/devenv) ]]; then
   echo '/mnt/devenv already mounted'
else
   mkdir -p /mnt/devenv
   echo '/dev/disk/by-id/scsi-0DO_Volume_devenv /mnt/devenv ext4 defaults,nofail,discard 0 0' | sudo tee -a /etc/fstab
   mount /mnt/devenv
fi

mkdir -p ${VOLUME}/jenkins
mkdir -p ${VOLUME}/postgresql
mkdir -p ${VOLUME}/sonar/data

# ln -s -f ${HOME_DIRECTORY}/jenkins/workspace ${VOLUME}/jenkins

apt-get install -y certbot

if [ -d ${VOLUME}/letsencrypt/ ]; then
   echo 'letsencripty directory exists'
else   
   certbot certonly -v --standalone -d jenkins.nakedelement.co.uk -d sonar.nakedelement.co.uk -d registry.nakedelement.co.uk -d baget.nakedelement.co.uk --non-interactive --agree-tos -m hello@finmytea.co.uk
   mv /etc/letsencrypt/ ${VOLUME}
fi

chown --recursive ${USERNAME}:${USERNAME} ${HOME_DIRECTORY}/
chown --recursive ${USERNAME}:docker ${HOME_DIRECTORY}/registry
chown --recursive ${USERNAME}:docker /etc/letsencrypt
chown --recursive ${USERNAME}:docker ${VOLUME}/

/usr/local/bin/docker-compose -f ${HOME_DIRECTORY}/dev-pipeline/docker-compose.yml up -d


# Jenkins Password: more /mnt/devenv/jenkins/secrets/initialAdminPassword

# Logs: sudo tail -f /var/log/cloud-init-output.log
