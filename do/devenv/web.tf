resource "digitalocean_app" "devenv_static" {
    spec {
         domain {
          name = "devenv.nakedelement.co.uk"
          type = "PRIMARY"
          zone = "nakedelement.co.uk"
        }

        name    = "devenv-static"
        region  = "ams"

        static_site {
            environment_slug = "html"
            name             = "devenv-static"
            source_dir       = "/"

            github {
                branch         = "main"
                deploy_on_push = true
                repo           = "pjgrenyer/devenv-static"
            }

            routes {
                path = "/"
            }
        }
    }
}