package test

import (
	"crypto/tls"
	"testing"
	"time"

	http_helper "github.com/gruntwork-io/terratest/modules/http-helper"
	"github.com/gruntwork-io/terratest/modules/terraform"
)

func TestApp(t *testing.T) {
	opts := &terraform.Options{TerraformDir: "../"}
	terraform.InitAndApply(t, opts)

	maxRetries := 20
	timeBetweenRetries := 30 * time.Second

	validate200 := func(status int, body string) bool {
		return 200 == status
	}

	validate403 := func(status int, body string) bool {
		return 403 == status
	}

	validate401 := func(status int, body string) bool {
		return 401 == status
	}

	http_helper.HttpGetWithRetryWithCustomValidation(t, "https://devenv.nakedelement.co.uk", &tls.Config{}, maxRetries, timeBetweenRetries, validate200)
	http_helper.HttpGetWithRetryWithCustomValidation(t, "https://jenkins.nakedelement.co.uk/", &tls.Config{}, maxRetries, timeBetweenRetries, validate403)
	http_helper.HttpGetWithRetryWithCustomValidation(t, "https://sonar.nakedelement.co.uk/", &tls.Config{}, maxRetries, timeBetweenRetries, validate200)
	http_helper.HttpGetWithRetryWithCustomValidation(t, "https://registry.nakedelement.co.uk/v2/_catalog", &tls.Config{}, maxRetries, timeBetweenRetries, validate401)

}
