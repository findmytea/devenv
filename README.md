# Dev Env #

A docker compose project which starts and stops a complete dev environment


### How do I get set up? ###

* Install Docker:
* Install Docker compose
* Run setup.sh
* Run mkdir.sh
* Run docker-compose up -d
* Run docker-compose down (to stop)

* apt install build-essential